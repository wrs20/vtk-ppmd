from threading import Thread
import numpy as np
import vtk
import signal

class PlotByPositions:
    def __init__(self, positions, radius=1.0):
        self.positions = positions

        self.N = positions.group.npart
        self.radius = radius
        # create a rendering window and renderer
        self.ren = vtk.vtkRenderer()
        self.renWin = vtk.vtkRenderWindow()
        self.renWin.AddRenderer(self.ren)
        # create a renderwindowinteractor
        self.iren = vtk.vtkRenderWindowInteractor()
        self.iren.SetRenderWindow(self.renWin)

        # sources
        self.sources = [vtk.vtkSphereSource() for ix in range(self.N)]
        self.actors = [vtk.vtkActor() for ix in range(self.N)]
        self.mappers = [vtk.vtkPolyDataMapper() for ix in range(self.N)]
        for px, sx in enumerate(self.sources):
            sx.SetCenter(*tuple(self.positions[px, 0:3:]))
            sx.SetRadius(self.radius)
            self.mappers[px].SetInputConnection(sx.GetOutputPort())
            self.actors[px].SetMapper(self.mappers[px])
            self.actors[px].GetProperty().SetColor((1.0,1.0,1.0))
            self.ren.AddActor(self.actors[px])
        self.running = False
        self.thread = None
        
        signal.signal(signal.SIGTERM, self._int_handler)
        signal.signal(signal.SIGINT, self._int_handler)

    def init(self):
        pass

    def _update_positions(self,obj,event):
        for px, ax in enumerate(self.actors):
            ax.SetPosition(*tuple(self.positions[px, :]))
        iren = obj
        iren.GetRenderWindow().Render()
        if not self.running:
            self._stop()

    def start(self):
        assert not self.running
        self.running = True
        self.thread = Thread(target=self._start)
        self.thread.start()

    def _start(self):
        # enable user interface interactor
        self.renWin.Render()
        self.iren.Initialize()
        self.iren.AddObserver('TimerEvent', self._update_positions)
        timerId = self.iren.CreateRepeatingTimer(10)
        self.iren.Start()
    
    def _stop(self):
        self.renWin.Finalize()
        self.iren.TerminateApp()

    def stop(self):
        self.running = False
        if self.thread is not None:
            self.thread.join()

    def _int_handler(self, signum, frame):
        self.stop()
        raise Exception

    def __del__(self):
        self.stop()
        self._stop()



