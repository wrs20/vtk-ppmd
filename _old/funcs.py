import math


def cart_to_spherical(cx, cy, cz):
    r = math.sqrt(cx*cx + cy*cy + cz*cz)
    theta = math.acos(float(cz)/r)
    phi = math.atan2(float(cy),float(cx))
    return r, theta, phi


def foo_sin(*args):
    r = args[0]
    theta = args[1]
    phi = args[2]
    val = math.sin(theta)*math.sin(phi)*0.25 + 0.5

    #print args, val
    return val

def foo_exp(*args):
    r = args[0]
    theta = args[1]
    phi = args[2]
    width = 8.
    val = math.exp(-1.*width*(phi*phi + (theta-0.5*math.pi)**2.0))

    #print args, val
    return val

def foo_exp_warp(*args):
    val = (foo_exp(*args))
    val += 0.0


    return val




