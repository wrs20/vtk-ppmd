



import numpy as np
import vtk

N = 100
E = 5
pos = np.random.uniform(low=-0.5*E, high=0.5*E, size=(N, 3))
col = np.random.uniform(low=0.0, high=1., size=(N, 3))

point_radius = 0.1

# create a rendering window and renderer
ren = vtk.vtkRenderer()
renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(ren)

# create a renderwindowinteractor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)

# create source

sources = [vtk.vtkSphereSource() for ix in range(N)]
actors = [vtk.vtkActor() for ix in range(N)]
mappers = [vtk.vtkPolyDataMapper() for ix in range(N)]
for px, sx in enumerate(sources):
    sx.SetCenter(*tuple(pos[px, 0:3:]))
    sx.SetRadius(point_radius)
    mappers[px].SetInputConnection(sx.GetOutputPort())
    actors[px].SetMapper(mappers[px])
    actors[px].GetProperty().SetColor(*tuple(col[px, 0:3:]))
    ren.AddActor(actors[px])


# enable user interface interactor
iren.Initialize()
renWin.Render()
iren.Start()

