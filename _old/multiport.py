import vtk


def main():
    '''One render window, multiple viewports'''
    iren_list = []
    rw = vtk.vtkRenderWindow()
    iren = vtk.vtkRenderWindowInteractor()
    iren.SetRenderWindow(rw)
    # Define viewport ranges
    xmins = [0, .5, 0, .5]
    xmaxs = [0.5, 1, 0.5, 1]
    ymins = [0, 0, .5, .5]
    ymaxs = [0.5, 0.5, 1, 1]


    # Create a sphere
    sphereSource = vtk.vtkSphereSource()
    sphereSource.SetCenter(0.0, 0.0, 0.0)
    sphereSource.SetRadius(5)
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputConnection(sphereSource.GetOutputPort())
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    camera = vtk.vtkCamera()

    camera.SetPosition(0, 0, 20)
    camera.SetFocalPoint(0,0,0)

    for i in range(4):

        ren = vtk.vtkRenderer()
        ren.SetActiveCamera(camera)
        rw.AddRenderer(ren)
        ren.SetViewport(xmins[i], ymins[i], xmaxs[i], ymaxs[i])

        # Create a mapper and actor

        ren.AddActor(actor)
        ren.ResetCamera()

    rw.Render()
    rw.SetWindowName('RW: Multiple ViewPorts')
    iren.Start()


if __name__ == '__main__':
    main()