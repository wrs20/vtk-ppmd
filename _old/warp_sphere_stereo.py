



import numpy as np
import vtk

from funcs import cart_to_spherical
from funcs import foo_exp_warp as foo

Npoints = 256
N = 100
E = 5
pos = np.random.uniform(low=-0.5*E, high=0.5*E, size=(N, 3))
col = np.random.uniform(low=0.0, high=1., size=(N, 3))

point_radius = 2.0

# create a rendering window and renderer


# create sphere
sphere = vtk.vtkSphereSource()
sphere_actor = vtk.vtkActor()


sphere_mapper = vtk.vtkPolyDataMapper()


sphere.SetCenter(0,0,0)
sphere.SetRadius(point_radius)
sphere.SetThetaResolution(Npoints)
sphere.SetPhiResolution(Npoints)


sphere.Update()
sphere_points = sphere.GetOutput().GetPoints()
num_points = int(sphere_points.GetNumberOfPoints())

print "num_points", num_points

scalars=vtk.vtkFloatArray()
scalars.SetNumberOfComponents(1)
for sx in range(num_points):
    #scalars.InsertNextTuple3(*sphere_points.GetPoint(sx))
    scalars.InsertNextTuple1(foo(*cart_to_spherical(*sphere_points.GetPoint(sx))))

sphere.GetOutput().GetPointData().SetScalars(scalars)

warp_sphere = vtk.vtkWarpScalar()
warp_sphere.SetInputConnection(sphere.GetOutputPort())
warp_sphere.SetScaleFactor(1.0)
warp_sphere.Update()

sphere_mapper.SetInputConnection(warp_sphere.GetOutputPort())
sphere_actor.SetMapper(sphere_mapper)



cameral = vtk.vtkCamera()
cameral.SetPosition(0, 0, 100)
cameral.SetFocalPoint(0, 0, 0)

camerar = vtk.vtkCamera()
camerar.SetPosition(0, 0, 110)
camerar.SetFocalPoint(0, 0, 0)

renl = vtk.vtkRenderer()
renl.SetActiveCamera(cameral)
renl.SetViewport(0.0, 0.0, 0.5, 1.0)
renl.AddActor(sphere_actor)
renl.ResetCamera()


renr = vtk.vtkRenderer()
renr.SetActiveCamera(camerar)
renr.SetViewport(0.5, 0.0, 1.0, 1.0)
renr.AddActor(sphere_actor)
renr.ResetCamera()

renWin = vtk.vtkRenderWindow()
renWin.AddRenderer(renl)
renWin.AddRenderer(renr)

# create a renderwindowinteractor
iren = vtk.vtkRenderWindowInteractor()
iren.SetRenderWindow(renWin)









axes = vtk.vtkOrientationMarkerWidget()
axes_actor = vtk.vtkAxesActor()
axes.SetOrientationMarker(axes_actor)
axes.SetInteractor(iren)
axes.SetEnabled( 1 )
axes.InteractiveOn()

# enable user interface interactor
iren.Initialize()
renWin.Render()
iren.Start()

